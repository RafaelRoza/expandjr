// const menu = document.querySelector(".navegacao");
// const menuItems = document.querySelectorAll(".link-navegacao");
// const hamburger= document.querySelector(".hamburger");
// const closeIcon= document.querySelector(".closeIcon");
// const menuIcon = document.querySelector(".menuIcon");

// function toggleMenu() {
//   if (menu.classList.contains("showMenu")) {
//     menu.classList.remove("showMenu");
//     closeIcon.style.display = "none";
//     menuIcon.style.display = "block";
//     menu.style.display = "none";
//   } else {
//     menu.classList.add("showMenu");
//     closeIcon.style.display = "block";
//     menuIcon.style.display = "none";
//     menu.style.display = "inherit";
//   }
// }

// hamburger.addEventListener("click", toggleMenu);

// menuItems.forEach( 
//   function(menuItem) { 
//     menuItem.addEventListener("click", toggleMenu);
//   }
// )

let menuIcon = document.querySelector('.container-menu-icon');
let menuHamburguer = document.querySelector('.menu-hamburguer');
let menuOptions = document.querySelector('.menu-options');

function myFunction(){
  menuIcon.classList.toggle("change");
  menuOptions.classList.toggle("changeMenu")
}

menuIcon.addEventListener('click', () => {
  myFunction();
})

window.addEventListener('resize', () => {
  if (window.innerWidth > 860){
    menuOptions.classList.remove("changeMenu")
    menuIcon.classList.remove("change")
  }
})
