let casesDiv = document.getElementById("cases");
let feedback = document.querySelectorAll(".case");
let casesDots = document.querySelectorAll(".case-dot")

let currentCase = 0;

function cases_carrossel(){

    if(currentCase  < 0){
        currentCase  = feedback.length - 1;
    }
    if(currentCase  > feedback.length - 1){
        currentCase  = 0;
    }

    casesDots.forEach((dot, index) => {
        dot.classList.remove("dot-active");
        if (index == currentCase) {
          dot.classList.add("dot-active");
        }
      });
    
    casesDiv.style.transform = `translateX(${-currentCase * (window.innerWidth * 0.7 + 20)}px)`;
}

// setInterval(cases_carrossel, 4000);

let prevCaseButton = document.querySelector('.prev-case');
let nextCaseButton = document.querySelector('.next-case');

prevCaseButton.addEventListener('click', () => {
    currentCase -= 1;
    cases_carrossel();
})

nextCaseButton.addEventListener('click', () => {
    currentCase += 1;
    cases_carrossel();
})

casesDots.forEach((dot, index) => {
    dot.addEventListener("click", () => {
        currentCase = index;
        cases_carrossel();
    });
  });

setInterval(() => {
    nextCaseButton.click();
}, 6000);

window.addEventListener('resize', () => {
    //Evita que "quebre" o codigo enquanto a janela é redimensionada
    currentCase = -1;
    nextCaseButton.click();
});