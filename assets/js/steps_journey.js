let infoP = document.querySelectorAll('.steps-info p');
let stepsButtons = document.querySelectorAll('.inner-circle');

function show_info(index){

    infoP.forEach(paragrafo => {
        paragrafo.style.display = 'none';
    });

    infoP[index].style.display = 'flex';

    stepsButtons.forEach((button, position) => {
        button.classList.remove("step-active");
        if (position == index) {
            button.classList.add("step-active");
        }
    });
}

stepsButtons.forEach((dot, index) => {
    dot.addEventListener("click", () => {
        show_info(index);
    });
  });

stepsButtons[0].click();