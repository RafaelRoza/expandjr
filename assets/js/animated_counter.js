let valores = document.querySelectorAll(".num");
let container = document.querySelectorAll("#showcase-resultados")
let intervalo = 1250;
/*animação dos numeros*/ 
function number_rising(){
    valores.forEach((valor) =>{
        let valorInicial = 0;
    let valorFinal = parseInt(valor.getAttribute("data-val"));
    let duracao = Math.floor(intervalo / valorFinal);
    let contator = setInterval(function(){
        valorInicial += 1;
        valor.textContent = valorInicial;
        if(valorInicial == valorFinal){
            clearInterval(contator)
        }
    }, duracao);
});
}
/*scrolling iniciando a animação*/
let ob = new IntersectionObserver((entries) =>{
    entries.forEach((entry) =>{
        if(entry.isIntersecting){
            number_rising();
        }
    })
}, 
{threshold:0.3}
);

for(let i = 0; i < container.length; i++){
    const el = container[i];
    ob.observe(el);

}
