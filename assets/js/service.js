  // Adiciona evento de clique para cada cartão
const cartoes = document.querySelectorAll('.card');

for (let i = 0; i < cartoes.length; i++) {
    cartoes[i].addEventListener('click', function() {
        const modalID = `modal${i+1}`; // Cria ID do modal correspondente
        iniciaModal(modalID); // Chama a função iniciaModal com o ID do modal correspondente
    });
}

function iniciaModal(modalID){
    const modal = document.getElementById(modalID);
    const modalX = modal.querySelector('.modal-x');
    let container = document.querySelector('.container-modal');
    let divContainer = document.querySelector('.modal-conteiner');

    modal.classList.add('mostrar');
    modalX.addEventListener('click', function() {
        modal.classList.remove('mostrar');
        container.style.display = "none";
    });

    document.addEventListener('click', (event) => {
        if(!divContainer.contains(event.target) && container.contains(event.target)){
            modalX.click();
        }
    });

    container.style.display = "flex";
    
}

// let 

// if (window.location.href){

// }