//MINHA

let partnersDiv = document.getElementById("partners");
let partner = document.querySelectorAll(".partner");
let partnerDots = document.querySelectorAll(".partner-dot");

let removeDots = 0;

//Adiciona a quantidade de dots de acordo com a tela
function verificar_screen(){
    for (let cont = 0; cont < partnerDots.length - 1; cont++){
        partnerDots[cont].style.display = 'none';
    }
    if(window.innerWidth > 1300){
        for (let cont = 0; cont < partnerDots.length - 4; cont++){
            partnerDots[cont].style.display = 'flex';
        }

        removeDots = 4;
    }

    else if(1000 < window.innerWidth && window.innerWidth <= 1300){
        for (let cont = 0; cont < partnerDots.length - 3; cont++){
            partnerDots[cont].style.display = 'flex';
        }

        removeDots = 3;
    }

    else if(600 < window.innerWidth && window.innerWidth <= 1000){
        for (let cont = 0; cont < partnerDots.length - 2; cont++){
            partnerDots[cont].style.display = 'flex';
        }

        removeDots = 2;
    }

    else if(window.innerWidth <= 600){
        for (let cont = 0; cont < partnerDots.length - 1; cont++){
            partnerDots[cont].style.display = 'flex';
        }

        removeDots = 1;
    }
}

verificar_screen();

let current_partner = 0;

//Movimenta o carrossel
function partners_carrossel(){

    if(current_partner < 0){
        current_partner = partner.length - removeDots;
    }
    if(current_partner > partner.length - removeDots){
        current_partner = 0;
    }

    partnerDots.forEach((dot, index) => {
        dot.classList.remove("dot-active");
        if (index == current_partner) {
          dot.classList.add("dot-active");
        }
      });
    
    partnersDiv.style.transform = `translateX(${-current_partner * 300}px)`;
}

let prevPartnerButton = document.querySelector('.prev-partner');
let nextPartnerButton = document.querySelector('.next-partner');

prevPartnerButton.addEventListener('click', () => {
    current_partner -= 1;
    partners_carrossel();
})

nextPartnerButton.addEventListener('click', () => {
    current_partner += 1;
    partners_carrossel();
})

partnerDots.forEach((dot, index) => {
    dot.addEventListener("click", () => {
        current_partner = index;
        partners_carrossel();
    });
});

setInterval(() => {
    nextPartnerButton.click();
}, 2000);

//Verifica se houve redisionamento de janela e executa a função
window.addEventListener('resize', () => {
    //Evita que "quebre" o codigo enquanto a janela é redimensionada
    current_partner = -1;
    nextPartnerButton.click();

    verificar_screen()
});