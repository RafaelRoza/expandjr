<?php
    get_header();
?>
<main>
<section class='inicio' style="<?php
if(empty(get_field('background-home'))){
    echo ("background-color: #0076C1");
}else{
    echo ("background-image: url(" . get_field('background-home') . ")");
}
?>"
>
    <h1 class='titulo'>Expanda seus horizontes</h1>
    <h2 class ='subtitulo'>através das nossas soluções internacionais de alto impacto</h2>
    <a id='form-button' href="<?php echo get_site_url(); ?>/contato/">Fale com um especialista</a>
</section>
<section class='showcase' id="showcase-servicos">
    <h2 >Nossos serviços</h2>
    <div id='servicos'>
        <div id='para-voce'>
            <h2>Para você</h2>
            <div class='card'>
                <img src=
                "
                <?php
                    if(empty(get_field('dupla_cidadania'))){
                        echo (IMAGES_DIR . '/circle.png');
                    }else{
                        echo (get_field('dupla_cidadania'));
                    }
                ?>
                "
                alt="dupla_cidadania">
                <h3>Dupla Cidadania Portuguesa</h3>
            </div>
            <div class='card'>
            <img src=
                "
                <?php
                    if(empty(get_field('emissao_de_passaporte'))){
                        echo (IMAGES_DIR . '/circle.png');
                    }else{
                        echo (get_field('emissao_de_passaporte'));
                    }
                ?>
                "
                alt="emissão de passaporte">
                <h3>Acessoria para emissão de passaporte brasileiro</h3>
            </div>
        </div>
        <div id='linha-vertical'>
        </div>
        <div id = 'para-empresa'>
            <h2>Para sua Empresa</h2>
            <div class='linha'>
                <div class ='card'>
                    <img src=
                    "
                    <?php
                        if(empty(get_field('analise_burocratica'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('analise_burocratica'));
                        }
                    ?>
                    "
                    alt="analise_burocratica">
                    <h3>Analise Burocrática</h3>
                </div>
                <div class ='card'>
                    <img src=
                    "
                    <?php
                        if(empty(get_field('estudo_e_analise_de_mercado'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('estudo_e_analise_de_mercado'));
                        }
                    ?>
                    "
                    alt="estudo_e_analise_de_mercado">
                    <h3>Estudo e Análise de Mercado</h3>
                </div>
            </div>
            <div class='linha'>
                    <div class ='card'>
                        <img src=
                        "
                        <?php
                            if(empty(get_field('planejamento_logistico'))){
                                echo (IMAGES_DIR . '/circle.png');
                            }else{
                                echo (get_field('planejamento_logistico'));
                            }
                        ?>
                        " 
                        alt="planejamento logistico">
                        <h3>Planejamento Logístico</h3>
                    </div>
                    <div class ='card'>
                        <img src=
                        "
                        <?php
                            if(empty(get_field('prospeccao_internacional'))){
                                echo (IMAGES_DIR . '/circle.png');
                            }else{
                                echo (get_field('prospeccao_internacional'));
                            }
                        ?>
                        "
                        alt="prospeccao_internacional">
                        <h3>Prospecção Internacional</h3>
                    </div>
            </div>
    </div>
</section>
<section id ='showcase-resultados'class='showcase'>
    <h2>Resultados</h2>
    <div id ='resultados'>
        <div class='card'>
        <img src=
        "
        <?php
            if(empty(get_field('anos_de_mercado'))){
                echo (IMAGES_DIR . '/circle.png');
            }else{
                echo (get_field('anos_de_mercado'));
            }
        ?>
        "
        alt="anos de mercado">
            <div class='contador'><span class='num' data-val='<?php echo get_field('n1')?>'>0</span></div>
            <h3>ANOS DE MERCADO</h3>
        </div>
        <div class = 'card'>
            <img src=
            "
            <?php
                if(empty(get_field('projetos_realizados'))){
                    echo (IMAGES_DIR . '/circle.png');
                }else{
                    echo (get_field('projetos_realizados'));
                }
                ?>
            " 
            alt="projetos">
            <div class='contador'>+<span class='num' data-val="<?php echo get_field('n2')?>">0</span></div>
            <h3>PROJETOS REALIZADOS</h3>
        </div>
        <div class='card'>
            <img src=
            "
            <?php
                if(empty(get_field('estudantes_impactados'))){
                    echo (IMAGES_DIR . '/circle.png');
                }else{
                    echo (get_field('estudantes_impactados'));
                }
            ?>
            " 
            alt="formandos">
            <div class='contador'>+<span class='num' data-val='<?php echo get_field('n3')?>'>0</span></div>
            <h3>ESTUDANTES IMPACTADOS</h3>
        </div>
        <div class='card'>
            <img src=
            "
            <?php
                if(empty(get_field('satisfacao_de_mercado'))){
                    echo (IMAGES_DIR . '/circle.png');
                }else{
                    echo (get_field('satisfacao_de_mercado'));
                }
                ?>
            "
            alt="avaliação">
            <div class='contador'>+<span class='num' data-val='<?php echo get_field('n4')?>'>0</span>%</div>
            <h3>SATISFAÇÃO DE MERCADO</h3>
        </div>
    </div>
</section>

    <section class="successful-cases">
        <h2>Cases de Sucesso</h2>

        <div class="cases-buttons">
            <button class="prev-case carrossel-button" type="button"><img src="<?= IMAGES_DIR . '/prev-button.png';?>" alt=""></button>
            
            <div class="container-cases">
                <div class="cases" id="cases">
                    <div class="case">
                        <p class="case-text">"<?php echo get_field('feedback_1');?>"</p>
                        <div class="feedback-author">
                            <img src="<?php echo get_field('imagem_primeiro');?>" alt="autor do feedback">
                            <p><?php echo get_field('autor_feedback_1');?></p>
                        </div>
                    </div>
                    <div class="case">
                        <p class="case-text">"<?php echo get_field('feedback_2');?>"</p>
                        <div class="feedback-author">
                            <img src="<?php echo get_field('imagem_segundo');?>" alt="autor do feedback">
                            <p><?php echo get_field('autor_feedback_2');?></p>
                        </div>
                    </div>
                    <div class="case">
                        <p class="case-text">"<?php echo get_field('feedback_3');?>"</p>
                        <div class="feedback-author">
                            <img src="<?php echo get_field('imagem_terceiro');?>" alt="autor do feedback">
                            <p><?php echo get_field('autor_feedback_3');?></p>
                        </div>
                    </div>
                    <div class="case">
                        <p class="case-text">"<?php echo get_field('feedback_4');?>"</p>
                        <div class="feedback-author">
                            <img src="<?php echo get_field('imagem_quarto');?>" alt="autor do feedback">
                            <p><?php echo get_field('autor_feedback_4');?></p>
                        </div>
                    </div>
                </div>
            </div>

            <button class="next-case carrossel-button" type="button"><img src="<?= IMAGES_DIR . '/next-button.png';?>" alt=""></button>
        </div>

        <div class="cases-dots">
            <span class="case-dot dot-active"></span>
            <span class="case-dot"></span>
            <span class="case-dot"></span>
            <span class="case-dot"></span>
        </div>
    </section>

    <section class="partners-section">
        <h2>Apoiadores</h2>

        <div class="container-and-buttons">
            <button class="prev-partner carrossel-button" type="button"><img src="<?= IMAGES_DIR . '/prev-button.png';?>" alt=""></button>
            <div class="partners-container">
                <div class="partners" id="partners">
                    <?php 
                    $totalSpans= 0;

                    if(have_rows('imagens_do_apoiadores')): ?>
                        <?php while(have_rows('imagens_do_apoiadores')):
                        ?>
                            <?php echo '<img class="partner" src="' . the_row()['full_image_url'] . '" alt="">';
                            $totalSpans += 1; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            
            
            </div>
            <button class="next-partner carrossel-button" type="button"><img src="<?= IMAGES_DIR . '/next-button.png';?>" alt=""></button>
        </div>

        <div class="partners-dots">
            <span class="partner-dot dot-active"></span>
            <?php
                 for($cont = 0; $cont < $totalSpans; $cont++):
                    echo '<span class="partner-dot"></span>';
                endfor
            ?>
        </div>
    </section>
    




    <div class="container-modal">
        <div id="modal1" class="modal-conteiner">
            <button class="modal-x"><img src="<?php echo IMAGES_DIR . '/close-popup.png'?>" alt="close popup"></button>

            <img src="
                    <?php
                        if(empty(get_field('dupla_cidadania'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('dupla_cidadania'));
                        }
                    ?>" alt="dupla_cidadania" class="modal-imagens">

            <h3 class="modal-titulo">Dupla Cidadania Portuguesa</h3>
            <p class="modal-descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id et voluptatum debitis velit ut alias, aliquam pariatur doloribus ipsam, quae optio labore accusantium a voluptatibus maxime reprehenderit aspernatur necessitatibus nulla.</p>
            <a href="<?php echo get_site_url(); ?>/contato/" class="modal-botao">Diagnóstico Gratuito</a>
        </div>
        <div id="modal2" class='modal-conteiner'>
            <button class="modal-x"><img src="<?php echo IMAGES_DIR . '/close-popup.png'?>" alt="close popup"></button>

            <img src="
                    <?php
                        if(empty(get_field('emissao_de_passaporte'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('emissao_de_passaporte'));
                        }
                    ?>" alt="emissao_de_passaporte" class="modal-imagens">

            <h3 class="modal-titulo">Acessoria para emissão de passaporte brasileiro</h3>
            <p class="modal-descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id et voluptatum debitis velit ut alias, aliquam pariatur doloribus ipsam, quae optio labore accusantium a voluptatibus maxime reprehenderit aspernatur necessitatibus nulla.</p>
            <a href="<?php echo get_site_url(); ?>/contato/" class="modal-botao">Diagnóstico Gratuito</a>
        </div>

        <div id="modal3" class='modal-conteiner'>
            <button class="modal-x"><img src="<?php echo IMAGES_DIR . '/close-popup.png'?>" alt="close popup"></button>

            <img src=" 
                    <?php
                        if(empty(get_field('analise_burocratica'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('analise_burocratica'));
                        }
                    ?>" alt="analise_burocratica" class="modal-imagens">

            <h3 class="modal-titulo">Analise Burocrática</h3>
            <p class="modal-descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id et voluptatum debitis velit ut alias, aliquam pariatur doloribus ipsam, quae optio labore accusantium a voluptatibus maxime reprehenderit aspernatur necessitatibus nulla.</p>
            <a href="<?php echo get_site_url(); ?>/contato/" class="modal-botao">Diagnóstico Gratuito</a>
        </div>
        
        <div id="modal4" class='modal-conteiner'>
            <button class="modal-x"><img src="<?php echo IMAGES_DIR . '/close-popup.png'?>" alt="close popup"></button>

            <img src="
                    <?php
                        if(empty(get_field('estudo_e_analise_de_mercado'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('estudo_e_analise_de_mercado'));
                        }
                    ?>" alt="estudo_e_analise_de_mercado" class="modal-imagens">

            <h3 class="modal-titulo">Estudo e Análise de Mercado</h3>
            <p class="modal-descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id et voluptatum debitis velit ut alias, aliquam pariatur doloribus ipsam, quae optio labore accusantium a voluptatibus maxime reprehenderit aspernatur necessitatibus nulla.</p>
            <a href="<?php echo get_site_url(); ?>/contato/" class="modal-botao">Diagnóstico Gratuito</a>
        </div>
        <div id="modal5" class='modal-conteiner'>
            <button class="modal-x"><img src="<?php echo IMAGES_DIR . '/close-popup.png'?>" alt="close popup"></button>

            <img src="
                    <?php
                        if(empty(get_field('planejamento_logistico'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('planejamento_logistico'));
                        }
                    ?>" alt="planejamento_logistico" class="modal-imagens">

            <h3 class="modal-titulo">Planejamento Logístico</h3>
            <p class="modal-descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id et voluptatum debitis velit ut alias, aliquam pariatur doloribus ipsam, quae optio labore accusantium a voluptatibus maxime reprehenderit aspernatur necessitatibus nulla.</p>
            <a href="<?php echo get_site_url(); ?>/contato/" class="modal-botao">Diagnóstico Gratuito</a>
        </div>
        
        <div id="modal6" class='modal-conteiner'>
            <button class="modal-x"><img src="<?php echo IMAGES_DIR . '/close-popup.png'?>" alt="close popup"></button>

            <img src="
                    <?php
                        if(empty(get_field('prospeccao_internacional'))){
                            echo (IMAGES_DIR . '/circle.png');
                        }else{
                            echo (get_field('prospeccao_internacional'));
                        }
                    ?>" alt="prospeccao_internacional" class="modal-imagens">

            <h3 class="modal-titulo">Prospecção Internacional</h3>
            <p class="modal-descricao">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id et voluptatum debitis velit ut alias, aliquam pariatur doloribus ipsam, quae optio labore accusantium a voluptatibus maxime reprehenderit aspernatur necessitatibus nulla.</p>
            <a href="<?php echo get_site_url(); ?>/contato/" class="modal-botao">Diagnóstico Gratuito</a>
        </div>
    </div>
</main>
<?php
    get_footer();
?>