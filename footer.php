
<footer class="site-footer">
  
<div class="caixa">  
  
  <div class="superior">

    <div class="coluna">
        <h3>Postagens Populares</h3>
        <div class="recent-posts">
          <?php  
            $popular_posts = wpp_get_mostpopular(array(
                'limit' => 3,
                'range' => 'all', 
                'post_type' => 'post',
                'stats_views' => true, 
            ));?>
        </div>
    </div>
  
    <div class="coluna">
        <h3>Postagens Recentes</h3>


        <div class="recent-posts">
          <?php
              $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              $args = array(
                  'post_type' => 'post',
                  'posts_per_page' => 3,
                  'paged' => $paged
              );
              $query = new WP_Query($args);
              if ($query->have_posts()) :
                  while ($query->have_posts()) :
                      $query->the_post();
                  ?>
                  <a href="<?php echo get_permalink();?>"><?php the_title();?></a>
                  <?php endwhile;
            endif;?>
        </div>
    </div>

    <div class="coluna">
      <h3>Links</h3>
      <div class="links">
        <a class="link-navegacao" href="<?php echo esc_url( home_url( '/' ) ); ?>#showcase-servicos">Serviços</a>
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/quem-somos/">Quem Somos</a>
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/blog/">Blog</a>
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/politica-de-privacidade/">Política de privacidade</a>
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/termos-de-uso/">Termos de uso</a>
      </div>
      
    </div>  
  </div>


  <div class="inferior">
    <div class="inferior1">
      <h3>EXPAND JR</h3>
      <p>CNPJ: 11.111.111/0000-00</p>
    </div>
    
    <div class="campo-email">
      <p>Cadastre-se em nossa newsletter!</p>
      <form method="post" action="http://expandjr5.local/?na=s">
        <input type="email" name="ne" value="" placeholder="Digite seu e-mail" required>
        <label class="checkbox">
        <input type="checkbox" name="ny" required>
        <a target="_blank" href="http://expandjr5.local/privacy-policy/">Li e aceito as políticas de privacidade e termos de uso</a>
        </label>
        <br>
        <button type="submit"  value="Subscribe">Cadastrar</button>
      </form>
    </div>
   
    <div class="contato">
      <p><?php echo get_field('endereco', 43); ?></p> 
      <p><?php echo get_field('telefone', 43); ?></p>

      <div class="redessociais">

        <a href="<?php echo get_field('instagram', 43); ?>">
          <img src="<?= IMAGES_DIR . '/instagram.png';?>" alt="Instagram">
        </a>
        <a href="<?php echo get_field('linkedin', 43); ?>">
          <img src="<?= IMAGES_DIR . '/linkedin.png';?>" alt="Linkedin">
        </a>
        <a href="<?php echo 'mailto:' . get_field('email', 43); ?>">
          <img src="<?= IMAGES_DIR . '/email.png';?>" alt="E-mail">
        </a>
      </div>
    </div>
  </div>
  
  <div class="final">
    <p>Copyright ⒸExpand JR 2023</p>
  </div>
</div>

</footer>

  <?php wp_footer(); ?>

