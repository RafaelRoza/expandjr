<?php
get_header();
?>

<main class="main-single">
<section>
    <?php
    if (have_posts()){
        while(have_posts()){
            the_post();
            $id_post = get_the_ID();
            $img_post_url = wp_get_attachment_url(get_post_thumbnail_id($id_post));
            ?>
            <?php the_title();?>
            <img src="<?php echo $img_post_url?>" alt="">
            <?php the_content(); ?>
    
        <?php
        }
    }?>
</section>
<section class="search-section">
            <!-- <h3 class="search-bar"><?php get_search_form();?></h3> -->
            <form action="<?php bloginfo('url');?>/" method="GET">
                <input class="search-bar" type="text" name="s" id="s" placeholder="Buscar">
                
            </form>

            <div class="category-filter">
                <h3>Categorias</h3>

                <?php
                $categorias = get_categories();
                foreach($categorias as $categoria){
                    ?>
                    <a class="category-search" href="<?php echo get_term_link($categoria->term_id); ?>" ><?php echo $categoria->name; ?><img src="<?php echo(IMAGES_DIR . '/icone-pasta.png')?>" alt="icone de pasta"></a>
                    <?php
                }
                ?>
            </div>
        </section>
</main>

<?php
get_footer();
?>
