<?php
get_header();
?>
<main class="main-blog">
    <section id='vitrine-blog' style="<?php
        if(empty(get_field('background-blog'))){
            echo ("background-color: #0076C1");
        }else{
            echo ("background-image: url(" . get_field('background-blog') . ")");
            }
        ?>"
        >
        <h1>NOSSO BLOG</h1>
        <p>Nos conectamos para conectar o mundo</p>
    </section>

    <div class="blog-search">
        <section class="posts-section">
        <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'paged' => $paged
            );
            $query = new WP_Query($args);

            if ($query->have_posts()) :
                while ($query->have_posts()) :
                    $query->the_post();
                    $id_post = get_the_ID();
                    $img_post_url = wp_get_attachment_url(get_post_thumbnail_id($id_post));
                    $css_image_post = 'background-image: url('. $img_post_url .');';  
                ?>

                    <div class="front-post">
                        <div class="post-image" style="<?= $css_image_post;?>"></div>

                        <div class="post-info">
                            <div class="title-category">
                                <h3 class="post-title"><?php the_title();?></h1>
                                <h4 class="category-post"><?php echo the_category();?></h4>
                            </div>
                            
                            <p class="post-abstract"><?php echo wp_trim_words(get_the_content(), 50);?></p>
                            <a class = "page-post-link" href="<?php echo get_permalink(); ?>">Ler mais</a>
                        </div>
                    </div>

                <?php endwhile;
            endif;

            $pagination_args = array(
                'base' => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
                'total' => $query->max_num_pages,
                'current' => max(1, $paged),
                'prev_text' => ('« Anterior'),
                'next_text' => ('Próximo »'),
            );
            
            wp_reset_postdata();?>
        </section>

        <section class="search-section">
            <!-- <h3 class="search-bar"><?php get_search_form();?></h3> -->
            <form action="<?php bloginfo('url');?>/" method="GET">
                <input class="search-bar" type="text" name="s" id="s" placeholder="Buscar">
                
            </form>

            <div class="category-filter">
                <h3>Categorias</h3>

                <?php
                $categorias = get_categories();
                foreach($categorias as $categoria){
                    ?>
                    <a class="category-search" href="<?php echo get_term_link($categoria->term_id); ?>" ><?php echo $categoria->name; ?><img src="<?php echo(IMAGES_DIR . '/icone-pasta.png')?>" alt="icone de pasta"></a>
                    <?php
                }
                ?>
            </div>
        </section>
    </div>
    
    <p class="pagination"><?php echo paginate_links($pagination_args);?></p>
</main>
<?php
get_footer()
?>