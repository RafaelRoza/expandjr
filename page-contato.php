<?php
get_header();
?>

    <main class="main-contact">
        <section id='vitrine' style="<?php
        if(empty(get_field('background-contatos'))){
            echo ("background-color: #0076C1");
        }else{
            echo ("background-image: url(" . get_field('background-contatos') . ")");
            }
        ?>"
        >
            <h1>CONTATOS</h1>
            <p>Fale conosco e peça seu diagnóstico gratuito</p>
        </section>

        <section class="journey">
            <h2>Nossa Jornada do Cliente</h2>

            <div class="journey-steps">
                <h3>Primeiro contato</h3>
                <h3>Diagnóstico</h3>
                <h3>Negociação</h3>
                <h3>Início do projeto</h3>
            </div>

            <div class="steps-buttons">
                <div class="step-circle step-left">
                    <div class="outer-circle">
                        <div class="inner-circle"></div>
                    </div>
                </div>

                <div class="step-circle">
                    <div class="outer-circle">
                        <div class="inner-circle"></div>
                    </div>
                </div>

                <div class="step-circle">
                    <div class="outer-circle">
                        <div class="inner-circle"></div>
                    </div>
                </div>

                <div class="step-circle step-right">
                    <div class="outer-circle">
                        <div class="inner-circle"></div>
                    </div>
                </div>

            </div>

            <div class="steps-info">
                <p>Primeiro contato: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati blanditiis repellendus corporis dicta totam non cumque autem ut maxime est, architecto hic eligendi fuga! Velit quos mollitia officia natus. In. orem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus amet vel quos! Tempore illo esse asperiores doloremque, non dolorem saepe. Dolor inventore fugiat distinctio reprehenderit magni optio consectetur nesciunt.</p>

                <p>Diagnostico: Lorem, ipsum dolor sit amet consectetur adipisicing elit. Enim eos repellat iste ab odio officiis ratione dolorem labore explicabo reprehenderit a impedit suscipit assumenda ducimus praesentium velit, est nesciunt vero? Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque molestias non sapiente enim. At officia error asperiores labore pariatur aliquam, voluptatum quasi qui adipisci provident, ullam, id mollitia! Quia, dolorum?</p>

                <p>Negociação: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati blanditiis repellendus corporis dicta totam non cumque autem ut maxime est, architecto hic eligendi fuga! Velit quos mollitia officia natus. In. orem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus amet vel quos! Tempore illo esse asperiores doloremque, non dolorem saepe. Dolor inventore fugiat distinctio reprehenderit magni optio consectetur nesciunt.</p>

                <p>Inicio do projeto: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias porro, nesciunt, consequuntur quisquam ut aliquid, ab ullam ipsa dolor voluptatem possimus blanditiis eius! Id nulla, laboriosam ex nemo suscipit odio. Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio temporibus delectus exercitationem molestias omnis itaque doloribus consectetur, nostrum quibusdam iste odit, non sapiente ut ex nesciunt saepe necessitatibus consequuntur cupiditate.</p>
            </div>
        </section>
        
        <div class="info-and-form">
            <section class = 'info-contact'>
                <h2>Informações de contato</h2>
                <div class = 'info'>
                    <img src="<?php echo(IMAGES_DIR . '/telefone-icon.png')?>" alt="telefone icon">
                    <p><?php the_field('telefone');?></p>
                </div>
                <div class='info'>
                    <img src="<?php echo(IMAGES_DIR . '/email-icon.png')?>" alt="email icon">
                    <p><?php the_field('email');?></p>
                </div>
                <div class='info'>
                    <img src="<?php echo(IMAGES_DIR . '/endereco-icon.png')?>" alt="endereço icon">
                    <p><?php the_field('endereco');?></p>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3673.9427526655804!2d-43.17663418577341!3d-22.952335345266608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99801d8685ded1%3A0x69b71c9241d45826!2sAv.%20Pasteur%2C%20250%20-%205-A%20-%20Urca%2C%20Rio%20de%20Janeiro%20-%20RJ%2C%2022290-902!5e0!3m2!1spt-BR!2sbr!4v1678228016877!5m2!1spt-BR!2sbr" width="400" height=" 300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </section>

            <section class="form-contact">
                <h2 class="form-title">Formulário de contato</h2>           
                <?php
                if (have_posts()){
                    while(have_posts()){
                        the_post();
                        ?>
                        <main>
                            <?php the_content(); ?>
                        </main>
        
                    <?php
                    }
                }?>
            </section>
        </div>
    </main>

<?php
get_footer();
?>
