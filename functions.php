<?php

// variaveis
define('ROOT_DIR', get_theme_file_path());
define('STYLES_DIR', get_template_directory_uri() . '/assets/css');
define('IMAGES_DIR', get_template_directory_uri() . '/assets/images');
define('INCLUDES_DIR', ROOT_DIR . '/includes');
define('SCRIPTS_DIR', get_template_directory_uri() . '/assets/js');

// includes
include_once(INCLUDES_DIR . '/enqueue.php');


// ganchos
add_action('wp_enqueue_scripts', 'expand_jr_enqueue');
add_theme_support('post-thumbnails');


?>