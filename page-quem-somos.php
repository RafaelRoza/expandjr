
<?php get_header(); ?>

    <section class='quemsomosvitrine' style="<?php
        $background = get_field('background_quem_somos');
        if(!empty($background)){
            echo ("background-image: url(" . $background['url'] . ")");
        }else{
            echo ("background-color: #0076C1");
            }
        ?>"
        >
        <h1>QUEM SOMOS</h1>
        <p>Conheça um pouco da nossa história</p>
    </section>

<main class='quemsomosmain'>

    <section class="container">

        <div class="card">
            <div class="imgBx"
                <?php 
                    $image = get_field('main_img_01');
                    if( !empty( $image ) ){ ?>
                        style = "background-image: url(<?php echo $image['url']?>);"
                    <?php 
                    } else{
                        ?>
                        style = "background-image: url(<?php echo IMAGES_DIR . '/img01.png';?>);"
                        <?php
                    }
                ?>>
                               
            </div>
            <div class="content">
                <?php 
                    $title = get_field('titulo_01');
                    if( !empty( $title ) ){ ?>
                    <h2><?php echo $title?></h2>
                    <?php
                    }
                ?>
                <?php 
                    $text = get_field('texto_01');
                    if( !empty( $text ) ){ ?>
                    <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
        </div>

        <div class="card-sec">
            <div class="imgBx-sec"
                <?php 
                    $image = get_field('sec_img_01');
                    if( !empty( $image ) ){ ?>
                        style = "background-image: url(<?php echo $image['url']?>);"
                    <?php 
                    } else{
                        ?>
                        style = "background-image: url(<?php echo IMAGES_DIR . '/img01.png';?>);"
                        <?php
                    }
                     ?>>
            </div>
            <div class="content">
            <?php 
                    $text = get_field('texto_02');
                    if( !empty( $text ) ){ ?>
                    <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
        </div>

        <div class="card-sec">
            <div class="imgBx-sec"
                <?php 
                    $image = get_field('sec_img_02');
                    if( !empty( $image ) ){ ?>
                        style = "background-image: url(<?php echo $image['url']?>);"
                    <?php 
                    } else{
                        ?>
                        style = "background-image: url(<?php echo IMAGES_DIR . '/img01.png';?>);"
                        <?php
                    }
                     ?>>
            </div>
            <div class="content">
            <?php 
                    $text = get_field('texto_03');
                    if( !empty( $text ) ){ ?>
                    <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
        </div>
    </section>


    <section class="valores">
        <div class="valoresTitulo">
        <?php 
                $title = get_field('titulo_02');
                if( !empty( $title ) ){ ?>
                    <h2><?php echo $title?></h2>
                <?php
                }
            ?>
        </div>
        <div class="valoresComponentes">
            <div>
            <?php 
                $sub_title = get_field('subtitulo_01');
                if( !empty( $sub_title ) ){ ?>
                    <h3><?php echo $sub_title?></h3>
                <?php
                }
            ?>
                <?php 
                    $image = get_field('valor_img_01');
                    if( !empty( $image ) ){ ?>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" >
                    <?php 
                    } else{
                        ?>
                        <img src="<?php echo IMAGES_DIR . '/img01.png' ?>" alt="Imagem 01">
                        <?php
                    }

                    $text = get_field('texto_04');
                    if( !empty( $text ) ){ ?>
                        <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
            <div>
            <?php 
                $sub_title = get_field('subtitulo_02');
                if( !empty( $sub_title ) ){ ?>
                    <h3><?php echo $sub_title?></h3>
                <?php
                }
            ?>
                <?php 
                    $image = get_field('valor_img_02');
                    if( !empty( $image ) ){ ?>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" >
                    <?php 
                    } else{
                        ?>
                        <img src="<?php echo IMAGES_DIR . '/img01.png' ?>" alt="Imagem 01">
                        <?php
                    }

                    $text = get_field('texto_05');
                    if( !empty( $text ) ){ ?>
                        <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
            <div>
            <?php 
                $sub_title = get_field('subtitulo_03');
                if( !empty( $sub_title ) ){ ?>
                    <h3><?php echo $sub_title?></h3>
                <?php
                }
            ?>
                <?php 
                    $image = get_field('valor_img_03');
                    if( !empty( $image ) ){ ?>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" >
                    <?php 
                    } else{
                        ?>
                        <img src="<?php echo IMAGES_DIR . '/img01.png' ?>" alt="Imagem 01">
                        <?php
                    }

                    $text = get_field('texto_06');
                    if( !empty( $text ) ){ ?>
                    <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
        </div>
    </section>

    <section class="container">

        <div class="card">
            <div class="content">
            <?php 
                $title = get_field('titulo_03');
                if( !empty( $title ) ){ ?>
                    <h2><?php echo $title?></h2>
                <?php
                }
            ?>
                <?php 
                    $text = get_field('texto_07');
                    if( !empty( $text ) ){ ?>
                    <p><?php echo nl2br($text)?></p>
                    <?php
                    }
                ?>
            </div>
            <div class="imgBx"
                <?php 
                    $image = get_field('main_img_02');
                    if( !empty( $image ) ){ ?>
                        style = "background-image: url(<?php echo $image['url']?>);"
                    <?php 
                    } else{
                        ?>
                        style = "background-image: url(<?php echo IMAGES_DIR . '/img01.png';?>);"
                        <?php
                    }
                ?>>
            </div>
        </div>
    </section>











 


</main>

<?php get_footer(); ?>
