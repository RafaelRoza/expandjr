<?php 

function expand_jr_enqueue(){
    //Register
    wp_register_style('expand-jr-reset', STYLES_DIR . '/reset.css', [], '1.0.0', false);
    wp_register_style('header-expand', STYLES_DIR . '/header.css', [], '1.0.0', false);
    wp_register_style('footer-expand', STYLES_DIR . '/footer.css', [], '1.0.0', false);
    wp_register_style('home-style', STYLES_DIR . '/page-home.css', [], '1.0.0', false);
    wp_register_style('contato-style', STYLES_DIR . '/page-contato.css', [], '1.0.0', false);
    wp_register_style('blog-style', STYLES_DIR . '/page-blog.css',[], '1.0.0', false);
    wp_register_style('fontes-style', 'https://fonts.googleapis.com/css2?family=Barlow:wght@500;700&family=Inter:wght@400;600;700&family=Roboto:wght@400;500;700&display=swap', [], '1.0.0', false);
    wp_register_style('quem-somos-style', STYLES_DIR . '/quem-somos.css',[], '1.0.0', false);
    wp_register_style('single-style', STYLES_DIR . '/single.css',[], '1.0.0', false);

    wp_register_script('cases-carrossel-js', SCRIPTS_DIR . '/cases_carrossel.js', [], '1.0.0', true);
    wp_register_script('partners-carrossel-js', SCRIPTS_DIR . '/partners_carrossel.js', [], '1.0.0', true);
    wp_register_script('steps-journey-js', SCRIPTS_DIR . '/steps_journey.js', [], '1.0.0', true);
    wp_register_script('animated-counter-js', SCRIPTS_DIR . '/animated_counter.js', [], '1.0.0', true);
    wp_register_script('service-pop-up-js', SCRIPTS_DIR . '/service.js', [], '1.0.0', true);
    wp_register_script('menu-hamburguer-js', SCRIPTS_DIR . '/menu_hamburguer.js', [], '1.0.0', true);
    //Enqueue
    wp_enqueue_style('expand-jr-reset');
    wp_enqueue_style('header-expand');
    wp_enqueue_style('footer-expand');
    wp_enqueue_style('home-style');
    wp_enqueue_style('contato-style');
    wp_enqueue_style('blog-style');
    wp_enqueue_style('fontes-style');
    wp_enqueue_style('quem-somos-style');
    wp_enqueue_style('single-style');

    wp_enqueue_script('cases-carrossel-js');
    wp_enqueue_script('partners-carrossel-js');
    wp_enqueue_script('steps-journey-js'); 
    wp_enqueue_script('animated-counter-js');
    wp_enqueue_script('steps-journey-js');
    wp_enqueue_script('service-pop-up-js');  
    wp_enqueue_script('menu-hamburguer-js');  
    
}


?>