<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php wp_title(); ?></title>
  <?php wp_head(); ?>
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
  <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
</head>
<body <?php body_class(); ?>>
 
<header>
    <div class="logo">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img src="<?= IMAGES_DIR . '/expandjr_logo.png';?>" alt="Logo da Expand Jr">
      </a>
    </div>

  <div class="navegacao-tradutor">
    <!-- <div class="hamburger">
      <i class="menuIcon material-icons"><img src="<?= IMAGES_DIR . '/menu.png';?>" alt="menu"></i>
      <i class="closeIcon material-icons"><img src="<?= IMAGES_DIR . '/close.png';?>" alt="close"></i>
    </div> -->
    
    <div class="navegacao">
      <a class="link-navegacao" href="<?php echo get_site_url(); ?>/quem-somos/">Quem Somos</a>
      <a class="link-navegacao" href="<?php echo esc_url( home_url( '/' ) ); ?>#showcase-servicos">Serviços</a>
      <a class="link-navegacao" href="<?php echo get_site_url(); ?>/blog/">Blog</a>
      <a class="link-navegacao" href="<?php echo get_site_url(); ?>/contato/">Contato</a>
    </div>

    <div class="tradutor">
      <?php echo do_shortcode('[gtranslate]'); ?>
    </div>

    <div class="menu-hamburguer">
      <div class="container-menu-icon">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
      </div>

      <div class="menu-options">
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/quem-somos/">Quem Somos</a>
        <a class="link-navegacao" href="<?php echo esc_url( home_url( '/' ) ); ?>#showcase-servicos">Serviços</a>
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/blog/">Blog</a>
        <a class="link-navegacao" href="<?php echo get_site_url(); ?>/contato/">Contato</a>
        <p class="tradutor-menu"><?php echo do_shortcode('[gtranslate]'); ?></p>
      </div>
    </div>

  </div>
</header>
